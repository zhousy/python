# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""
import util
from game import Directions

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first
    [2nd Edition: p 75, 3rd Edition: p 87]

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm
    [2nd Edition: Fig. 3.18, 3rd Edition: Fig 3.7].

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    #print "Start:", problem.getStartState()
    #print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    #print "Start's successors:", problem.getSuccessors(problem.getStartState())
    
    if problem.isGoalState(problem.getStartState()) :
        return [Directions.STOP]
    
    #search core data structure
    searchStruc=util.Stack()
    
    preNode={}
    preAction={}
    
    visited=[]
    resNode=[]
    flag=True
    res=[]
    #initializing
    visited.append(problem.getStartState())
    for succ in problem.getSuccessors(problem.getStartState()):
        searchStruc.push(succ)
        preNode[succ[0]]=problem.getStartState()
        preAction[succ[0]]=succ[1]
        if problem.isGoalState(succ[0]) :
            res.append(succ[1])
            return res
    
    while not searchStruc.isEmpty():
        currProc=searchStruc.pop()
        visited.append(currProc[0])
        for succ in problem.getSuccessors(currProc[0]):
            if (succ[0] not in visited)&(succ[0] not in preNode.keys()):
                searchStruc.push(succ)
                preNode[succ[0]]=currProc[0]
                preAction[succ[0]]=succ[1]
                if problem.isGoalState(succ[0]) :
                    resNode=succ[0]
                    while (resNode)!=(problem.getStartState()):
                        res.insert(0, preAction[resNode])
                        resNode=preNode[resNode]
                    return res

    return res
    util.raiseNotDefined()
    

def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first.
    [2nd Edition: p 73, 3rd Edition: p 82]
    """
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()) :
        return [Directions.STOP]
    
    #search core data structure
    searchStruc=util.Queue()
    
    preNode={}
    preAction={}
    
    visited=[]
    resNode=[]
    flag=True
    res=[]
    #initializing
    visited.append(problem.getStartState())
    for succ in problem.getSuccessors(problem.getStartState()):
        searchStruc.push(succ)
        preNode[succ[0]]=problem.getStartState()
        preAction[succ[0]]=succ[1]
        if problem.isGoalState(succ[0]) :
            res.append(succ[1])
            return res
    
    while not searchStruc.isEmpty():
        currProc=searchStruc.pop()
        visited.append(currProc[0])
        for succ in problem.getSuccessors(currProc[0]):
            if (succ[0] not in visited)&(succ[0] not in preNode.keys()):
                searchStruc.push(succ)
                preNode[succ[0]]=currProc[0]
                preAction[succ[0]]=succ[1]
                if problem.isGoalState(succ[0]) :
                    resNode=succ[0]
                    while (resNode)!=(problem.getStartState()):
                        res.insert(0, preAction[resNode])
                        resNode=preNode[resNode]
                    return res

    return res
    util.raiseNotDefined()

def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()) :
        return [Directions.STOP]
    
    #search core data structure
    searchStruc=util.PriorityQueue()
    
    preNode={}
    preAction={}
    cost={}
    visited=[]
    resNode=[]
    flag=True
    res=[]
    #initializing
    visited.append(problem.getStartState())
    cost[problem.getStartState()]=0
    for succ in problem.getSuccessors(problem.getStartState()):
        cost[succ[0]]=succ[2]
        preNode[succ[0]]=problem.getStartState()
        preAction[succ[0]]=succ[1]
        searchStruc.push(succ,succ[2])

    
    while not searchStruc.isEmpty():
        currProc=searchStruc.pop()
        visited.append(currProc[0])
        if problem.isGoalState(currProc[0]) :
            resNode=currProc[0]
            while (resNode)!=(problem.getStartState()):
                res.insert(0, preAction[resNode])
                resNode=preNode[resNode]
            return res
        for succ in problem.getSuccessors(currProc[0]):
            if ((succ[0] not in visited)&(succ[0] not in preNode.keys()))or((succ[0] not in visited)&(cost[succ[0]]>cost[currProc[0]]+succ[2])):

                cost[succ[0]]=cost[currProc[0]]+succ[2]
                preNode[succ[0]]=currProc[0]
                preAction[succ[0]]=succ[1]
                searchStruc.push(succ,cost[succ[0]])
                if problem.isGoalState(succ[0]) :
                    resNode=succ[0]
                    while (resNode)!=(problem.getStartState()):
                        res.insert(0, preAction[resNode])
                        resNode=preNode[resNode]
                    return res
    return res
    util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()) :
        return [Directions.STOP]
    
    #search core data structure
    searchStruc=util.PriorityQueue()
    
    preNode={}
    preAction={}
    cost={}
    visited=[]
    resNode=[]
    flag=True
    res=[]
    #initializing
    visited.append(problem.getStartState())
    cost[problem.getStartState()]=0
    for succ in problem.getSuccessors(problem.getStartState()):
        cost[succ[0]]=succ[2]
        preNode[succ[0]]=problem.getStartState()
        preAction[succ[0]]=succ[1]
        searchStruc.push(succ,succ[2]+heuristic(succ[0],problem))

    while not searchStruc.isEmpty():
        currProc=searchStruc.pop()
        visited.append(currProc[0])
        if problem.isGoalState(currProc[0]) :
            resNode=currProc[0]
            while (resNode)!=(problem.getStartState()):
                res.insert(0, preAction[resNode])
                resNode=preNode[resNode]
            return res
        for succ in problem.getSuccessors(currProc[0]):
            if ((succ[0] not in visited)&(succ[0] not in preNode.keys()))or((succ[0] not in visited)&(cost[succ[0]]>cost[currProc[0]]+succ[2])):

                cost[succ[0]]=cost[currProc[0]]+succ[2]
                preNode[succ[0]]=currProc[0]
                preAction[succ[0]]=succ[1]
                searchStruc.push(succ,cost[succ[0]]+heuristic(succ[0],problem))
                if problem.isGoalState(succ[0]) :
                    resNode=succ[0]
                    while (resNode)!=(problem.getStartState()):
                        res.insert(0, preAction[resNode])
                        resNode=preNode[resNode]
                    return res
    return res
    util.raiseNotDefined()
    

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
