# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()

    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (newFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = successorGameState.getPacmanPosition()
    newFood = successorGameState.getFood()
    newGhostStates = successorGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    "*** YOUR CODE HERE ***"
    
    #adjust these 2 para when performance is poor
    minD=2
    maxD=100
    
    res = max(util.manhattanDistance(currentGameState.getGhostPosition(1), newPos), minD) + successorGameState.getScore()
    disList=[maxD]
    for foodpos in newFood.asList():
        disList.append(util.manhattanDistance(foodpos, newPos))
    res -= minD * min(disList)
    if successorGameState.getPacmanPosition() in currentGameState.getCapsules():
        res += 2*maxD
    if (currentGameState.getNumFood() > successorGameState.getNumFood()):
        res += maxD
    if action == Directions.STOP:
        res -= minD
    return res
    #return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """
    "*** YOUR CODE HERE ***"
    maxNumber = 99999
    
    def maxFunction(gameState, depth):
        depth = depth + 1
        if (depth == self.depth) or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        res = [-1 * maxNumber]
        actionList = gameState.getLegalActions(0)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            res.append(minFunction(gameState.generateSuccessor(0, action), depth, 1))
        return max(res)

    def minFunction(gameState, depth, agents):
        if depth == self.depth or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        res = [maxNumber]
        actionList=gameState.getLegalActions(agents)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            if agents ==  gameState.getNumAgents() - 1 :
                res.append(maxFunction(gameState.generateSuccessor(agents, action), depth))
            else:
                res.append(minFunction(gameState.generateSuccessor(agents, action), depth, agents+1))
        return min(res)
    
    res = Directions.STOP
    temp = -1*maxNumber
    actionList=gameState.getLegalActions(0)
    if Directions.STOP in actionList :
        actionList.remove(Directions.STOP)
    for action in actionList:
        score = minFunction(gameState.generateSuccessor(0, action), 0, 1)
        if score > temp :
            temp = score
            res = action
    return res

class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """


  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"
    maxNumber = 99999
    
    def maxFunction(gameState, alpha, beta, depth):
        depth = depth + 1
        if (depth == self.depth) or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        actionList = gameState.getLegalActions(0)
        res = -1 * maxNumber
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            res = max(res,minFunction(gameState.generateSuccessor(0, action), alpha, beta, depth, 1))
            if res >= beta :
                return res
            alpha = max(res,alpha)
        return res

    def minFunction(gameState, alpha, beta, depth, agents):
        if depth == self.depth or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        actionList = gameState.getLegalActions(agents)
        res = maxNumber
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            if agents == gameState.getNumAgents() - 1:
                res=min(res,maxFunction(gameState.generateSuccessor(agents, action), alpha, beta, depth))
            else:
                res=min(res,minFunction(gameState.generateSuccessor(agents, action), alpha, beta, depth, agents + 1))
            if res<=alpha :
                return res
            beta=min(res,beta)
        return res
      
    score = -1 * maxNumber
    alpha = score
    beta = maxNumber
    res = Directions.STOP
    actionList = gameState.getLegalActions(0)
    if Directions.STOP in actionList :
        actionList.remove(Directions.STOP)
    for action in actionList:
        temp = minFunction(gameState.generateSuccessor(0, action), alpha, beta, 0, 1)
        if temp > score :
            score = temp
            res = action
        if temp >= beta :
            return res
        alpha = max(score,alpha)
    return res
    util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    maxNumber = 99999
    
    def maxFunction(gameState, depth):
        depth = depth + 1
        if (depth == self.depth) or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        res = [-1 * maxNumber]
        actionList=gameState.getLegalActions(0)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            res.append(minFunction(gameState.generateSuccessor(0, action), depth, 1))
        return max(res)

    def minFunction(gameState, depth, agents):
        if depth == self.depth or gameState.isWin() or gameState.isLose() :
            return self.evaluationFunction(gameState)
        res = 0
        actionList = gameState.getLegalActions(agents)
        length = len(actionList)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            if agents == gameState.getNumAgents() - 1:
                res += (maxFunction(gameState.generateSuccessor(agents, action), depth))*1.0/length
            else:
                res += (minFunction(gameState.generateSuccessor(agents, action), depth, agents + 1))*1.0/length
        return res
    res = Directions.STOP
    temp = -1*maxNumber
    actionList = gameState.getLegalActions(0)
    if Directions.STOP in actionList :
        actionList.remove(Directions.STOP)
    for action in actionList:
        score = minFunction(gameState.generateSuccessor(0, action), 0, 1)
        if score > temp :
            temp = score
            res = action
    return res    
    util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    
    The former evaluation function just makes some preference to the pacman, such as like eating scared ghosts and hate stop.
    This one try to make it more. Pacman always want to make the decision that the ghost will last scared longer, 
        like far from ghosts and near to the food.
    
    """
    "*** YOUR CODE HERE ***"
    newFood = currentGameState.getFood().asList()
    newGhostStates = currentGameState.getGhostStates()
    ghostDistances = min([manhattanDistance(x.getPosition(),currentGameState.getPacmanPosition()) for x in newGhostStates])
    foodDistances = [manhattanDistance(x,currentGameState.getPacmanPosition()) for x in newFood]
    if foodDistances == [] :
        foodDistances = [1]
    return currentGameState.getScore() + sum([ghostState.scaredTimer for ghostState in newGhostStates]) + ghostDistances/min(foodDistances)

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
  """
    Your agent for the mini-contest
  """

  def getAction(self, gameState):
    """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
    "*** YOUR CODE HERE ***"
    maxNumber = 99999
    
    def maxFunction(gameState, depth):
        depth = depth + 1
        if (depth == self.depth) or gameState.isWin() or gameState.isLose() :
            return betterEvaluationFunction(gameState)
        res = [-1 * maxNumber]
        actionList=gameState.getLegalActions(0)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            res.append(minFunction(gameState.generateSuccessor(0, action), depth, 1))
        return max(res)

    def minFunction(gameState, depth, agents):
        if depth == self.depth or gameState.isWin() or gameState.isLose() :
            return betterEvaluationFunction(gameState)
        res = 0
        actionList = gameState.getLegalActions(agents)
        length = len(actionList)
        if Directions.STOP in actionList :
            actionList.remove(Directions.STOP)
        for action in actionList:
            if agents == gameState.getNumAgents() - 1:
                res += (maxFunction(gameState.generateSuccessor(agents, action), depth))*1.0/length
            else:
                res += (minFunction(gameState.generateSuccessor(agents, action), depth, agents + 1))*1.0/length
        return res
    res = Directions.STOP
    temp = -1*maxNumber
    actionList = gameState.getLegalActions(0)
    if Directions.STOP in actionList :
        actionList.remove(Directions.STOP)
    for action in actionList:
        score = minFunction(gameState.generateSuccessor(0, action), 0, 1)
        if score > temp :
            temp = score
            res = action
    return res    
    util.raiseNotDefined()

